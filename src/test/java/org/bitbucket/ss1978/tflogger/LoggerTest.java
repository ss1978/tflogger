package org.bitbucket.ss1978.tflogger;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by sanya on 2016.05.27..
 */
public class LoggerTest {
    private static final Logger LOGGER = Logger.newLogger();
    //private static final java.util.logging.Logger JULLOGGER = java.util.logging.Logger.getLogger(LoggerTest.class.getName());

    public static void main(String... args) throws InterruptedException {
        new LoggerTest().infoTest();
    }

    @Test
    public void infoTest() throws InterruptedException {
        ExecutorService cp = Executors.newCachedThreadPool();
        for (int i = 0; i < 100; i++) {
            final int v = i;
            cp.submit(new Runnable() {
                public void run() {
                    LOGGER.enter("hello");
                    for (int ii = 0; ii < 1000; ii++)
                        LOGGER.info("Valami " + v + " " + ii);
                    LOGGER.error("Hiba", new IllegalArgumentException(new IllegalStateException()));
                    LOGGER.exit("hello");
                }
            });
        }

        cp.shutdown();
        cp.awaitTermination(2, TimeUnit.MINUTES);
        System.out.println("----VÉGE----");
    }

}