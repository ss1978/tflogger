package org.bitbucket.ss1978.tflogger;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by sanya on 2016.05.27..
 */
public final class LogEvent {
    public final String className;
    public final Object[] arguments;
    public final Level level;
    public final String location;
    public final String message;
    public final String stackTrace;
    public final long threadId;
    public final String threadName;
    public final long time;

    public LogEvent(String className, Level level, String valami, Object[] arguments, Throwable ex) {
        this.className = className;
        this.level = level;
        this.message = valami;
        this.arguments = arguments;
        this.time = System.currentTimeMillis();
        final Thread thread = Thread.currentThread();

        this.threadName = thread.getName();
        this.threadId = thread.getId();
        if ((Level.ENTER == level) || (Level.EXIT == level)) {
            final StackTraceElement callee = Thread.currentThread().getStackTrace()[3];

            this.location = callee.getFileName() + ":" + callee.getLineNumber();
        } else {
            location = null;
        }

        if (ex != null) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final PrintStream ps = new PrintStream(baos);

            ex.printStackTrace(ps);
            ps.flush();
            this.stackTrace = baos.toString();
        } else {
            this.stackTrace = null;
        }
    }


    enum Level {
        ERROR,
        WARNING,
        INFO,
        CONFIG,
        ENTER,
        EXIT,
        DEBUG,
        TRACE
    }
}
