package org.bitbucket.ss1978.tflogger;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by sanya on 2016.05.27.
 */
final class ThreadedLoggerService extends Thread implements LoggerService {
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private static final byte[] EOL = System.getProperty("line.separator").getBytes(UTF_8);
    private final LogEventFilter filter;
    private final LogFormatter formatter;
    private final OutputStream outputStream = new BufferedOutputStream(System.out);
    private final BlockingQueue<LogEvent> queue;
    private volatile boolean run = true;

    ThreadedLoggerService(final LogFormatter formatter, final LogEventFilter filter) {
        super("ThreadedLoggerService");
        setDaemon(true);
        this.formatter = formatter;
        queue = new LinkedBlockingQueue();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                while (!queue.isEmpty()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                run = false;
            }
        });
        start();
        this.filter = filter;
    }

    public void log(final LogEvent logEvent) {
        if (filter.accept(logEvent)) {
            queue.add(logEvent);
        }
    }

    public void run() {
        LogEvent event;
        while (run) {
            try {
                event = queue.poll(1, TimeUnit.SECONDS);
                if (event != null) {
                    try {
                        outputStream.write(formatter.format(event));
                        outputStream.write(EOL);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    outputStream.flush();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
