package org.bitbucket.ss1978.tflogger;

/**
 * Created by sanya on 2016.05.27..
 */
public interface LoggerService {
    void log(LogEvent logEvent);
}
