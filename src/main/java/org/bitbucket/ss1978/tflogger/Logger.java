package org.bitbucket.ss1978.tflogger;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

/**
 * Created by sanya on 2016.05.27..
 */
public final class Logger {
    private static final LoggerService loggerService = newLoggerService(newFormatter(), newFilter());
    private final String className;

    public Logger(final String className) {
        this.className = className;
    }

    private static LogEventFilter newFilter() {
        return new LogEventFilter() {
            @Override
            public boolean accept(LogEvent logEvent) {
                return logEvent.level.compareTo(LogEvent.Level.EXIT) <= 0;

            }
        };
    }

    private static LoggerService newLoggerService(final LogFormatter logFormatter, final LogEventFilter filter) {
        return new ThreadedLoggerService(logFormatter, filter);
    }

    private static LogFormatter newFormatter() {

        return new DefaultLogFormatter();
    }

    public static Logger newLogger() {
        return new Logger(Thread.currentThread().getStackTrace()[2].getClassName());
    }

    public static Logger newLogger(final String logger) {
        return new Logger(logger);
    }

    public void info(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.INFO, message, args, null));
    }

    public void enter(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.ENTER, message, args, null));
    }

    public void exit(String message) {
        loggerService.log(new LogEvent(className, LogEvent.Level.EXIT, message, null, null));
    }

    public <T> T exit(String message, T ret) {
        loggerService.log(new LogEvent(className, LogEvent.Level.EXIT, message, new Object[]{ret}, null));
        return ret;
    }

    public <T extends Throwable> T exception(String message, T ret) {
        loggerService.log(new LogEvent(className, LogEvent.Level.EXIT, message, new Object[]{ret}, null));
        return ret;
    }

    public void config(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.CONFIG, message, args, null));
    }

    public void debug(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.DEBUG, message, args, null));
    }

    public void warning(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.WARNING, message, args, null));
    }

    public void error(String message, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.ERROR, message, args, null));
    }

    public void error(String message, Throwable ex, Object... args) {
        loggerService.log(new LogEvent(className, LogEvent.Level.ERROR, message, args, ex));
    }

    static class DefaultLogFormatter implements LogFormatter {
        private static final Charset UTF_8 = Charset.forName("UTF-8");
        private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss:SSSS");

        public byte[] format(LogEvent event) {
            final StringBuilder sb = new StringBuilder() //
                    .append(simpleDateFormat.format((event.time)))
                    .append(" [").append(event.level).append("] ")
                    .append(event.threadName).append("[").append(event.threadId).append("]")
                    .append(" ").append(event.className).append(" ");
            if (event.location != null) {
                sb.append(event.location).append(" ");
            }
            sb.append(String.format(event.message, event.arguments));
            if (event.stackTrace != null) {
                sb.append(" ").append(event.stackTrace);
            }
            return sb.toString().getBytes(UTF_8);
        }
    }
}
