package org.bitbucket.ss1978.tflogger;

/**
 * Created by sanya on 2016.05.27..
 */
public interface LogEventFilter {
    boolean accept(LogEvent logEvent);
}
